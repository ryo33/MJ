MJ
==
chatbot!
###In Twitter
Working:  
--[japanese](https://twitter.com/mj_bot_jp)  
Planing:  
--english
###Description
MJ is a chatbot which can be used in any languages
###Requirement
Python3  
SQLite3
###Usage
```
import mj as MJ
mj = MJ.MJ()
mj.memory(["MJ", "is", "a", "chatbot", "which", "can", "be", "used", "in", "any", "language"])
print(mj.talk())
```
###License
  see [License](LICENSE)
###Author
  [ryo33](https://github.com/ryo33/ "ryo33's github page")
